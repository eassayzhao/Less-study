# CSS预处理语言——LESS

LESSCSS是一种动态样式语言，简称LESS，是CSS预处理语言的一种。

Less中文网http://lesscss.cn/

Less快速入门https://less.bootcss.com/

## 1.Less基本用法

传统CSS

```html
<style>
    .d1{
        width: 200px;
        height: 200px;
        border: 1px solid blue;
    }
    .d2{
        width: 100px;
        height: 100px;
        border: 1px solid blue;
    }
</style>
<body>
    <div class="d1">div1</div>
    <div class="d2">div2</div>
</body>
```

上面代码均为两个div添加了蓝色边框，若想同时修改边框颜色为红色，需要在.d1和.d2类样式里分别修改。这里是两个div，若更多div，修改的工作量实在太大。使用Less省时又省力。

### 1.1 直接使用

浏览器只识CSS，不识LESS。所有LESS文件最终都要转换为CSS文件在浏览器中使用。

传统CSS代码写在html文件的style标签中，其实，style标签有类型，默认类型type=“text/css"。直接使用Less需要在style标签中指明less类型。

- 内部less：

```html
<style type="type/less">
</style>
```

- 外部less：

```html
<link rel="stylesheet/less" href="less/01.less">
```

将上面的less代码写在01.less文件中。

写好的less代码，不能直接展示在浏览器，需要手段将其转换为css，less官网为我们提供了一个less.min.js文件，可以将less转换为css。我们只需要下载并引入文件。

http://lesscss.cn/#download-options链接里点击下载即可。（有时候显示无法访问，可以去这个链接下载，版本V2.5.3）

```html
<style type="type/less">
	@color:green;
	.d1{
        width: 200px;
        height: 200px;
        border: 1px solid @color;
    }
    .d2{
        width: 100px;
        height: 100px;
        border: 1px solid @color;
    }
</style>
<!--引入less.min.js文件 -->
<script src="less.min.js的路径"></script>
```

上面的代码中定义了一个color变量，d1和d2都使用该变量定义边框颜色，要修改颜色，直接修改color变量取值即可。

缺点：这种直接使用的方法，需要外部文件将less转换为css，影响效率。

### 1.2 使用插件

less主要是为了方便程序员进行代码编写和修改，提高开发效率，浏览器真正需要的是转换后的css文件。VS code软件中提供了easy less插件，可以将我们写好的less文件直接生成转换后的同名css文件，默认两个文件在同一路径下。在html文件中只需要引用生成后的css文件即可。

```html
<link rel="stylesheet" href="less/01.css">
```

默认less文件和生成的css文件在同一文件夹，不方便我们进行代码管理。

方法：可以在VS code中修改插件，将生成的css文件统一放入上层css文件夹中。

在VS code中选择上方菜单栏的文件，点击首选项-设置，在左侧的扩展一栏可以找到安装的easy less插件，选择在setting.json中编辑。

添加”out“属性，属性值是生成的css文件放入的路径。

```json
{
    "explorer.confirmDelete": false,
    "auto-comment-blocks.disabledLanguages": [
    
    
    
    ],
    "liveServer.settings.donotShowInfoMsg": true,
    "explorer.confirmDragAndDrop": false,
    "git.ignoreMissingGitWarning": true,
    "less.compile": {
        "out": "../css/"
    }
}
```

## 2.Less语法

### 1.1 变量定义

变量的加载使用Lazy Loading模式，无需先定义再使用。

Less中对于变量名称不并严格，如@a-z，@22都是合法变量名称，但建议按规范起名。

变量不仅可用于值，还可以用于选择器、URL、样式属性、变量嵌套等。

- 简单变量

  Less中使用@符号定义变量

```less
@width: 100px;
@height: 200px;
.d1{
    width:@width;
    height:@height;
}
```

- 选择器变量

```less
@selector: s1;
.@{selector}{
    color: #foo;
}
```

- url变量

```less
@path:"../imgs";
.btn{
    background:url("@{path}/p1.jpg");
}
```

- 属性名变量

```less
@prop1: color;
.p1{
    @{prop1}:red;
}
```

注：除简单变量使用外，其余类型的变量使用都需要用{}将变量名括起来。

- 变量嵌套

```less
//写法一
@aaa:50px;
@bbb:aaa;
.box{
    color:@@bbb;
}
//写法二
@aaa:50px;
@bbb:@aaa;
.box{
    color:@bbb;
}
```

### 1.2 变量特性

- 变量作用域

变量具有两种作用域：全局变量、局部变量

```less
//color-全局变量
@color:red;
.p1{
    color:@color;
}
.p2{
    //局部变量line
    @line:underline;
    text-decoration:@line;
}
```

- 变量提升

变量提升也可以称为变量懒加载，即变量可以先使用再定义。

```less
.p3{
    font-family:@family;
}
@family:黑体;
```

- 变量隔离

```less
@ddd:underline;
.p3{
    text-decoration: @ddd;
    @ddd:overline; //不影响全局变量
}
.d1{
    text-decoration:@ddd; //@ddd:underline
}
```

### 1.3 混合

在less中，可以定义一些通用属性集为一个class，然后在另一个class中去调用这些属性。

- 无参混合

```less
.common{
    font-size: 30px;
    text-decoration: underline;
}
.d1{
    color:red;
    //直接调用.common
    .common;
}
```

- 带参混合

```less
.hello(@bgColor,@fontSize){
    background:@bgColor;
    font-size: @fontSize;
}
.d1{
    color:red;
    .hello(pink,20px);
}
.d2{
    color:blue;
    .hello(yellow,30px);
}
//可以给.hello的形参定义默认值，此时可以直接.hello()调用
.hello(@bgColor:#ccc,@fontSize:15px){
    background:@bgColor;
    font-size: @fontSize;
}
```

- 导引混合

导引混合可以进行条件的判断

```less
.world(@fontSize) when(@fontSize<=30){
    font-size: @fontSize;
}
.world(@fontSize) when(@fontSize>30){
    font-size: 30px;
}
.world(@fontSize){ // 总是会匹配
    color: red;
}
.d4{
    .world(20px); //匹配第一个
}
.d5{
    .world(65px); //匹配第二个
}
```

### 1.4 运算

任何数字、颜色或变量都可以参与运算，如：

```less
@base:5%;
@filler:@base*2;
@other:@base+@filler;
color:#888/4; //#222
background-color:@base-color+#111;
@var:1px+5; //6px
```

### 1.5 内置函数

- 单位函数unit

用来改变或移除数据单位。

```less
unit(5,px); //添加单位
unit(5px); //去除单位
unit(10px,rem); //10rem
```

- 单位转换函数convert

在单位类型一致的情况下进行转换，否则返回原始数据。

```less
convert(1500ms,s); //5s
convert(30deg,px);//30deg
convert(0.5rad,deg); //rad表示弧度，deg表示角度
```

- 颜色相关函数

```less
color(darkgray); //#a9a9a9将颜色名转为颜色值
darken(red,20%); //将颜色在原有基础上更暗
lighten(red,20%); //将颜色在原有基础上更亮
mix(red,blue,50%); //将两种颜色进行混合后的颜色
```

- 数据函数data-uri

将图片数据转换为字节码。

```less
height:100px;
background:url(../images/p1.jpg);
background:data-uri(../images/p1.jpg); //Byte64编码
```

- 长度函数length

```less
@list:"one","two","three","four";
@n:length(@list); //4
```

- 提取函数extract

获取一组值中指定位置的数据

```less
@list:"one","two","three","four";
@myBorder:1px solid green;
.d4{
    color:extract(@myBorder,3); //提取第3个字段
}
```

- 数学函数

包括：ceil、floor、sqrt、abs、sin、asin、cos、acos、tan、atan、pow、min、max等与Math类相同的函数。

```less
round(3.1415,2): 四舍五入,保留两位小数
ceil: 向上取整
percentage(0.45): 将数值转换为百分比45%
pi: 相当于Math.PI
mod: 取余数
```

## 3.Less嵌套与继承

### 1.1 嵌套

less 能以嵌套的方式编写层叠样式，层次关系明显。同样，也可以更细化地写CSS选择器进行样式定位。

html代码：

 ```html
<body>
    <div class="d1">
        <p id="p1">
            <span>p1</span>
        </p>
        <div class="d2">
            <span>d2</span>
        </div>
    </div>
</body>
 ```

less代码：

```less
.d1{
    border: 1px solid #ccc;
    width: 500px; //整个d1都适用
    #p1{
         background:pink; //只适用#p1
         span{
            color: blue; //只适用#p1里的span
            &:hover{  //  &相当于是this
                color: red;
            }
        }
    }
    .d2{
        background:#ccc;
        span{
            color: green;
        }
    }
}
```

### 1.2 继承

继承extend被附在选择器后面或放置在规则集（具体定于样式处），它看起来像是一个在关键字extend后具有可选参数的伪类。

两种用法：1.在规则内部  2.选择器后面

```less
.aa{
    color:red;
    font-size: 30px;
    font-weight: bold;
}
.bb{
    text-decoration: underline;
    background: pink;
    border: 1px solid #333;
}
// 两种写法：1.在规则内部 2.选择器后面
.d1{ 
    border: 1px solid #ccc;
    &:extend(.aa); //规则内部
}
.d1:extend(.aa){ //选择器后面
     border: 1px solid #ccc; 
}
// 多继承，即同时继承多个样式
.d2{
    height:200px;
    &:extend(.aa,.bb); // 以逗号隔开
}
// 继承所有
.d4{
    border: 1px solid #ccc;
    &:extend(.cc all); // 凡是包含.cc的选择器都继承
}
```

## 4.引入其他文件

可以使用@import导入其他文件（.less或.css）

如果导入的是.less文件，可以省略扩展名

```less
@import "variable.less";
@import "mixin"; // 可省略后缀名
@import "../css/07.css";
```

引入其他less文件，便可以在当前文件使用其他文件包含的变量。